import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { createPost } from '../actions/index';
import { Link } from 'react-router';

class PostsNew extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    onSubmit(post){
        this.props.createPost(post)
            .then(() => this.context.router.push('/'))
    }

    render() {
        function hasError(field) {
            return (field.touched && field.invalid) ? 'has-danger' : '';
        }
        function textHasError(field) {
            return (field.touched && field.invalid) ? 'has-danger' : '';
        }

        function getErrorMessage(field) {
            return field.touched ? field.error : '';
        }

        const { fields: { title, categories, content }, handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <h3>Create A New Post</h3>

                <div className={`form-group ${hasError(title)}`}>
                    <label>Title</label>
                    <input type="text" className="form-control" {...title} />
                    <div className={`help-text ${textHasError(title)}`}>
                        {getErrorMessage(title)}
                    </div>
                </div>

                <div className={`form-group ${hasError(categories)}`}>
                    <label>Categories</label>
                    <input type="text" className="form-control" {...categories} />
                    <div className={`help-text ${textHasError(categories)}`}>
                        {getErrorMessage(categories)}
                    </div>
                </div>

                <div className={`form-group ${hasError(content)}`}>
                    <label>Content</label>
                    <textarea className="form-control" {...content} />
                    <div className={`help-text ${textHasError(content)}`}>
                        {getErrorMessage(content)}
                    </div>
                </div>

                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
                <Link to="/" className="btn btn-danger">
                    Cancel
                </Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.title) {
        errors.title = 'Enter a title';
    }

    if (!values.categories) {
        errors.categories = 'Enter a category';
    }

    if (!values.content) {
        errors.content = 'Enter some content';
    }

    return errors;
}

export default reduxForm({
    form: 'PostsNewForm',
    fields: ['title', 'categories', 'content'],
    validate
}, null, { createPost })(PostsNew);